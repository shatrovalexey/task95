<?php

namespace App\Models;

use Illuminate\Http\Request ;
use Illuminate\Database\Eloquent\Model ;
use Illuminate\Database\Eloquent\Builder ;
use App\Exceptions\Validator as ValidatorException ;
use Validator ;

/**
* Базовая модель
*/
class ModelBase extends Model {
	/**
	* @var array $rules - правила валидации полей
	*/
	public $rules = [ ] ;

	/**
	* Вывод ошибок валидации
	*
	* @return array - список ошибок
	*/
    public function validate( ) : array {
		$rules = static::rules( ) ;
		$data = $this->attributesToArray( ) ;
		$validator = Validator::make( $data , $rules ) ;

		if ( ! empty( static::$attributeNames ) ) {
			$validator->setAttributeNames( static::$attributeNames ) ;
		}
		if ( ! $validator->fails( ) ) {
			return [ ] ;
		}

		return $validator->errors( )->toArray( ) ;
	}

	/**
	* Сохранение записи в БД
	*
	* @return bool
	* @throws ValidatorException
	*/
	public function save( array $options = [ ] ) : bool {
		if ( $errors = $this->validate( ) ) {
			throw new ValidatorException( $errors ) ;
		}

		return parent::save( ) ;
	}
}