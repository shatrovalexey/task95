<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\ModelBase ;

class UserList extends ModelBase {
    use HasFactory ;

	protected $table = 'user_list' ;
	public $timestamps = true ;
	protected $fillable = [ 'name' , 'email' , 'bdate' , ] ;

	/**
	* Валидация полей
	*
	* @return array
	*/
	public static function rules( ) : array {
		return [
			'name' => [ 'required' , 'string' , 'min:1' , 'max:255' , ] ,
			'email' => [ 'required' , 'email' , 'min:1' , 'max:255' , ] ,
			'bdate' => [ 'required' , 'date' , ] ,
		] ;
	}
}