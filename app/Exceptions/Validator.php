<?php
	namespace App\Exceptions ;

	class Validator extends \Exception {
		/**
		* Конструктор
		*
		* @param mixed $message
		* @param long $code
		* @param Throwable $previous
		*/
		public function __construct( $message , $code = null , Throwable $previous = null ) {
			$this->message = json_encode( $message ) ;
			$this->code = $code ;
			$this->previous = $previous ;
		}

		/**
		* Переопределим строковое представление объекта.
		*
		* @return string|null
		*/
		public function __toString( ) {
			foreach ( [ 'is_integer' , 'is_bool' , 'is_string' , 'is_float' , 'is_double' , 'is_long' , 'is_null' , ] as $function_name ) {
				if ( ! $function_name( $this->message ) ) {
					continue ;
				}

				return $this->message ;
			}

			return json_encode( $this->message , \JSON_PRETTY_PRINT | \JSON_UNESCAPED_UNICODE ) ;
		}
	}