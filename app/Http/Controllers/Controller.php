<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Routing\Controller as BaseController;
use App\Models\UserList ;
use Illuminate\Http\Request ;
use App\Exceptions\Validator as ValidatorException ;

class Controller extends BaseController {
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests ;

	public function index( ) {
		return view( 'index' ) ;
	}

	/**
	* @param Illuminate\Http\Request $request - объект запроса
	*/
	public function user_list_create( Request $request ) {
		$model = new UserList( ) ;
		try {
			$model->fill( $request->all( ) ) ;
			$model->save( ) ;
		} catch ( ValidatorException $exception ) {
			return [ 'errors' => json_decode( $exception->getMessage( ) ) , ] ;
		}

		return $model->id ;
	}

	public function user_list( ) {
		return UserList::orderBy( 'id' , 'DESC' )->get( ) ;
	}
}