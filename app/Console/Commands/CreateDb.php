<?php

namespace App\Console\Commands;

use Illuminate\Console\Command ;
use Illuminate\Support\Facades\DB ;

/**
* Создание БД
*/
class CreateDb extends Command {
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'db:create {name}' ;

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Создание БД';

     /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle( ) {
        $schemaName = config( 'database.connections.mysql.database' ) ;
        $charset = config( 'database.connections.mysql.charset' , 'utf8mb4' ) ;
        $collation = config( 'database.connections.mysql.collation' , 'utf8mb4_unicode_ci' ) ;

        config( [ 'database.connections.mysql.database' => null , ] ) ;

        DB::statement( "
CREATE DATABASE IF NOT EXISTS `{$schemaName}` CHARACTER SET $charset COLLATE $collation ;
		" );

        config( [ 'database.connections.mysql.database' => $schemaName , ] ) ;

        return 0 ;
    }
}