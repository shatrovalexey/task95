<?php

use Illuminate\Support\Facades\Route ;
use App\Http\Controllers\Controller;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get( '/' , [ Controller::class , 'index' , ] ) ;
Route::post( '/user_list/create' , [ Controller::class , 'user_list_create' , ] ) ;
Route::get( '/user_list' , [ Controller::class , 'user_list' , ] ) ;