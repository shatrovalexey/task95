<!DOCTYPE html>

<html>
	<head>
		<title>Task95</title>
		<meta charset="utf8">
		<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" type="text/css">
		<script src="https://code.jquery.com/jquery-3.6.0.min.js"></script>
		<style>
label {
	display: block ;
}
*[data-role="error"] {
	color: "red" ;
}
*[data-role="error"]:empty {
	display: none ;
}
		</style>
		<script>
jQuery( function( ) {
	jQuery( "[data-source]" ).each( ( $i , $item ) => {
		let $self = jQuery( $item ) ;
		let $item_node = $self.find( "[role=row]" ).remove( ).clone( true ) ;
		let $update = ( ) => {
			jQuery.ajax( {
				"type" : "GET" ,
				"url" : $self.attr( "data-source" ) ,
				"dataType" : "json" ,
				"success" : ( $data ) => {
					try {
						$self.empty( ) ;

						for ( let $data_item of $data ) {
							let $item_node_current = $item_node.clone( true ) ;

							for ( let $field_name in $data_item ) {
								$item_node_current.find( "[data-field='" + $field_name + "']" ).text( $data_item[ $field_name ] ) ;
							}

							$self.append( $item_node_current ) ;
						}
					} catch ( $exception ) {
						console.log( $exception ) ;
					}
				} ,
				"failure" : ( ) => console.log( $exception )
			} )
		} ;

		$update( ) ;

		setInterval( $update , parseInt( $self.attr( "data-update" ) ) ) ;
	} ) ;

	jQuery( "#user_list-form" ).submit( ( $evt ) => {
		let $self = jQuery( $evt.currentTarget ) ;

		jQuery.ajax( {
			"url" : $self.attr( "action" ) ,
			"type" : $self.attr( "method" ) ,
			"data" : $self.serialize( ) ,
			"dataType" : "json" ,
			"success" : ( $data ) => {
				try {
					for ( let $key in $data.errors ) {
						console.log( $data.errors[ $key ] ) ;
						$self.find( "[data-role='error'][data-field='" + $key + "']" ).text( $data.errors[ $key ].join( "\n" ) ) ;
					}
				} catch ( $exception ) {
					$self.get( 0 ).reset( ) ;
					$self.find( "[autofocus]" ).get( 0 ).focus( ) ;
				}
			} ,
			"failure" : ( ) => alert( "Ошибка отправки данных" ) ,
		} ) ;

		return false ;
	} ) ;
} ) ;
		</script>
	</head>
	<body>
		<form action="/user_list/create" method="POST" id="user_list-form">
			<input type="hidden" name="_token" id="token" value="{{ csrf_token() }}">
			<fieldset>
				<legend>
					<h1>Форма ввода</h1>
				</legend>
				<label>
					<span>Имя</span>
					<input name="name" required pattern="^[А-ЯЁ][а-яёА-ЯЁ ]*$" autofocus>
					<span data-role="error" data-field="name"></span>
				</label>
				<label>
					<span>E-mail</span>
					<input type="email" name="email" required>
					<span data-role="error" data-field="email"></span>
				</label>
				<label>
					<span>Дата рождения</span>
					<input type="date" name="bdate" required>
					<span data-role="error" data-field="bdate"></span>
				</label>
				<label>
					<span>создать</span>
					<input type="submit" required value="&rarr;">
				</label>
			</fieldset>
		</form>
		<h2>Список записей</h2>
		<p>В принципе, можно и web-socket, но уже и так 95е задание. Решил оставить на потом.
		<ul id="user_list" class="user_list" data-source="/user_list" data-update="5000">
			<li role="row">
				<span class="user_list-field">
					<span class="user_list-field-value" data-field="id"></span>
				</span>

				<span class="user_list-field">
					<span class="user_list-field-label">Имя</span>
					<span class="user_list-field-value" data-field="name"></span>
				</span>

				<span class="user_list-field">
					<span class="user_list-field-label">E-mail</span>
					<span class="user_list-field-value" data-field="email"></span>
				</span>

				<span class="user_list-field">
					<span class="user_list-field-label">Дата рождения</span>
					<span class="user_list-field-value" data-field="bdate"></span>
				</span>
			</li>
		</ul>
	</body>
</html>